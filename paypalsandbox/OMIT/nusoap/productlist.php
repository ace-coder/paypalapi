<?php

require_once "nusoap.php";

function getProd($category) {

    return join(",", array($category));


//    if ($category == "books") {
//          
//        return join(",", array( $category));
//    }
//    else {
//        return "No products listed under that category";
//    }
}

$server = new soap_server();
$server->configureWSDL("productlist", "urn:productlist");

$server->register("getProd", array("category" => "xsd:array"), array("return" => "xsd:array"), "urn:productlist", "urn:productlist#getProd", "rpc", "encoded", "Get a listing of products by category");

$server->service($HTTP_RAW_POST_DATA);
