<?php

require __DIR__ . '/../bootstrap.php';

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

session_start();

$payer = new Payer();
$payer->setPaymentMethod("paypal");

$item1 = new Item();
$item1->setName('Ground Coffee 40 oz')->setCurrency('USD')->setQuantity(1)->setPrice('7.50');
$item2 = new Item();
$item2->setName('Granola bars')->setCurrency('USD')->setQuantity(5)->setPrice('2.00');
$itemList = new ItemList();
$itemList->setItems(array($item1, $item2));

$details = new Details();
$details->setShipping('1.20')->setTax('1.30')->setSubtotal('17.50');


$amount = new Amount();
$amount->setCurrency("USD")->setTotal("20.00")->setDetails($details);


$transaction = new Transaction();
$transaction->setAmount($amount)->setItemList($itemList)->setDescription("Payment description");


$baseUrl = getBaseUrl(); 
$redirectUrls = new RedirectUrls();
$redirectUrls->setReturnUrl("$baseUrl/ExecutePayment.php?success=true")->setCancelUrl("$baseUrl/ExecutePayment.php?success=false");


$payment = new Payment();
$payment->setIntent("sale")->setPayer($payer)->setRedirectUrls($redirectUrls)->setTransactions(array($transaction));




try {
    $payment->create($apiContext);
} catch (PayPal\Exception\PPConnectionException $ex) {
    echo "Exception: " . $ex->getMessage() . PHP_EOL;
    var_dump($ex->getData());
    exit(1);
}



foreach ($payment->getLinks() as $link) {
    if ($link->getRel() == 'approval_url') {
        $redirectUrl = $link->getHref();
        break;
    }
}



$_SESSION['paymentId'] = $payment->getId();
if (isset($redirectUrl)) {
    header("Location: $redirectUrl");
    exit;
}
?>