<?php

require __DIR__ . '/../bootstrap.php';

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;

session_start();
$Payer = new Payer();
$Payer->setPaymentMethod("paypal");
 
$items = array();

//$item_name = 'XYZ';
//$item_price = '1.7';
//$item_quentity = 1;
//$item_currency = 'USD';
//$items[] = array(
//    'item_name' => $item_name,
//    'item_price' => number_format(round($item_price, 2), 2),
//    'item_quentity' => $item_quentity,
//    'item_subtotal' => $item_price * $item_quentity,
//    'item_currency' => $item_currency
//);

$item_name = 'Product2';
$item_price = '1.90';
$item_quentity = 1;
$item_currency = 'USD';
$items[] = array(
    'item_name' => $item_name,
    'item_price' => number_format($item_price,  2),
    'item_quentity' => $item_quentity,
    'item_subtotal' => $item_price * $item_quentity,
    'item_currency' => $item_currency
);
$item_name = 'Product 3';
$item_price = '1.91';
$item_quentity = 10;
$item_currency = 'USD';
$items[] = array(
    'item_name' => $item_name,
    'item_price' => number_format(round($item_price, 2), 2),
    'item_quentity' => $item_quentity,
    'item_subtotal' => $item_price * $item_quentity,
    'item_currency' => $item_currency
);




$tax = '0.5';
$shipping = '1';
$subtotal = 0;
$currency = 'USD';
$ListItem = array();
foreach ($items as $_item) {
    $Item = new Item();
    $Item->setName($_item['item_name'])->setCurrency('USD')->setQuantity($_item['item_quentity'])->setPrice($_item['item_price']); 
    $subtotal += $_item['item_subtotal'];
    array_push($ListItem, $Item);
} 

$subtotal =   number_format(round($subtotal, 2), 2);  ;
$tax = number_format(round($tax, 2), 2); 
$shipping = number_format(round($shipping, 2), 2);  
$grandTotal = $subtotal + $shipping + $tax; 
$grandTotal = number_format(round($grandTotal, 2), 2); 


$ItemList = new ItemList();
$ItemList->setItems($ListItem);
$Details = new Details();
$Details->setShipping($shipping)->setTax($tax)->setSubtotal($subtotal);
$Amount = new Amount();
$Amount->setCurrency($currency)->setTotal($grandTotal)->setDetails($Details);
$Transaction = new Transaction();
$Transaction->setAmount($Amount)->setItemList($ItemList)->setDescription("Payment description");

$baseUrl = getBaseUrl();
$redirectUrls = new RedirectUrls();
$redirectUrls->setReturnUrl("$baseUrl/ExecutePayment.php?success=true")->setCancelUrl("$baseUrl/ExecutePayment.php?success=false");


$payment = new Payment();
$payment->setIntent("sale")->setPayer($Payer)->setRedirectUrls($redirectUrls)->setTransactions(array($Transaction));

$payment->custom = "CUSTOM";
// print_r($payment);
// exit();
try {
    $payment->create($apiContext);
} catch (PayPal\Exception\PPConnectionException $ex) {
    echo "Exception: " . $ex->getMessage() . PHP_EOL;
    var_dump($ex->getData());
    exit(1);
}



foreach ($payment->getLinks() as $link) {
    if ($link->getRel() == 'approval_url') {
        $redirectUrl = $link->getHref();
        break;
    }
}



$_SESSION['paymentId'] = $payment->getId();

// print_r($redirectUrl);
// exit();
if (isset($redirectUrl)) {
    header("Location: $redirectUrl");
    exit;
}
?>